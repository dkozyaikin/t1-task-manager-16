package ru.t1.dkozyaikin.tm.service;

import ru.t1.dkozyaikin.tm.api.repository.ICommandRepository;
import ru.t1.dkozyaikin.tm.api.service.ICommandService;
import ru.t1.dkozyaikin.tm.model.Command;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

    @Override
    public void clearOutput() {
        System.out.print("\033\143");
    }

}
